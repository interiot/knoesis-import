package eu.interiot;
import com.bbn.parliament.jena.joseki.client.RemoteModel;
import com.hp.hpl.jena.rdf.model.Model;
import org.apache.jena.riot.RDFDataMgr;

import java.io.File;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("\nFull path to the (unpacked) knoesis dataset should be provided!\n");
            System.exit(1);
        }
        File folder = new File(args[0]);
        File[] listOfFiles = folder.listFiles();
        if (listOfFiles != null) {
            String sparqlEndpoint = "http://localhost:8089/parliament/sparql";
            String bulkEndpoint = "http://localhost:8089/parliament/bulk";
            RemoteModel rmParliament = new RemoteModel(sparqlEndpoint, bulkEndpoint);
            try {
                rmParliament.dropNamedGraph("http://knoesis");
                rmParliament.createNamedGraph("http://knoesis", true);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Model m;
            for (File file : listOfFiles) {
                if (file.isFile()) {
                    m = RDFDataMgr.loadModel(file.getAbsolutePath());
                    try {
                        System.out.println(file.getAbsolutePath());
                        rmParliament.insertStatements(m, "http://knoesis");
                    } catch (IOException e) {
                        System.out.println(file.getAbsolutePath());
                        e.printStackTrace();
                    }

                }
            }
        }
    }
}
