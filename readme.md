## Knoesis to Parliament
This simple utility imports _LinkedSensorData_ dataset into the [Parliament](http://parliament.semwebcentral.org/) triple store.

### How to build
To build a self-contained JAR file use the following Maven command:

```bash
mvn install:install-file compile assembly:single
```
The resulting JAR will be produced in the `target` subdirectory.

### Assumptions

The utility assumes that there is a working instance of `Parliament` available on [http://localhost:8089/parliament/](http://localhost:8089/parliament/), with a defined "named graph" `<http://knoesis>`.